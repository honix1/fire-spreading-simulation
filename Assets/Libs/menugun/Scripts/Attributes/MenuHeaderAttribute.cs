﻿using UnityEngine;
using System.Collections;
using System;

namespace MenuGun {
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class MenuHeaderAttribute : PropertyAttribute {
        public readonly string header;

        public MenuHeaderAttribute(string header) {
            this.header = header;
        }
    }
}