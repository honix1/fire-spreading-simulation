﻿using UnityEngine;
using System.Collections;
using System;

namespace MenuGun {
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class MenuRangeAttribute : PropertyAttribute {
        public readonly float min;
        public readonly float max;

        public MenuRangeAttribute(float min, float max) {
            this.min = min;
            this.max = max;
        }
    }
}