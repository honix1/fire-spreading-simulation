﻿using UnityEngine;
using System.Collections;
using System;

namespace MenuGun {
    [AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    public sealed class MenuIntrospectAttribute : PropertyAttribute {
    }
}