﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class MenuGunHub : MonoBehaviour {

        public bool activeByDefault = false;
        public KeyCode activateKey = KeyCode.Escape;
        public List<UnityEngine.Object> objects;

        private bool active = false;
        private Window protoWindow;
        private List<Window> windows = new List<Window>();

        void Start() {
            if (Environment.Version >= new Version(4, 0)) {
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            }

            active = activeByDefault;
            protoWindow = GetComponentInChildren<Window>(true);
            
            foreach (UnityEngine.Object obj in objects) {
                CreateWindow(obj);
            }

            foreach (MenuGunEntry mge in GameObject.FindObjectsOfType<MenuGunEntry>()) {
                CreateWindow(mge.component, mge.customIdentifier);
            }

            DefaultWindowsPlacement();

            protoWindow.gameObject.SetActive(false);

            UpdateWindowsActive();

            StartCoroutine(FitContents());
        }

        IEnumerator FitContents() {
            yield return new WaitForEndOfFrame();
            foreach (Window window in windows) {
                window.FitContent();
            }
        }

        void CreateWindow(UnityEngine.Object obj, string customIdentifier = "") {
            if (obj == null) return;

            Window window = Instantiate(protoWindow, transform);
            window.gameObject.SetActive(true);
            window.Link(obj);

            if (customIdentifier != string.Empty) {
                window.Identifier = customIdentifier;
            }

            windows.Add(window);
        }

        void DefaultWindowsPlacement() {
            DefaultWindowsPlacement(new Vector2(80, -80), initial: true);
        }

        void DefaultWindowsPlacement(Vector2 pos, bool initial) {
            Vector2 travel = pos;
            foreach (Window w in windows) {
                w.GetComponent<RectTransform>().anchoredPosition = travel;
                if (!initial) {
                    w.Move(0, 0);
                }

                travel += new Vector2(60, -30);
            }
        }

        void Update() {
            if (Input.GetKeyDown(activateKey)) {
                active = !active;
                UpdateWindowsActive();
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.R)) {
                DefaultWindowsPlacement(Input.mousePosition - Vector3.up * Screen.height, initial: false);
            }
        }

        private void UpdateWindowsActive() {
            foreach (Window w in windows) {
                w.gameObject.SetActive(active);
                if (active) {
                    w.RunFetcher();
                }
            }
        }

        public void DoTranslation(Func<string,string> trans) {
            foreach (Window w in windows) {
                foreach (Panel n in w.GetPanels()) {
                    n.SetName(trans(n.Identifier));
                }
            }
        }
    }
}