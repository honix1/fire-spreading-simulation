﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MenuGun {
    public class WindowResize : MonoBehaviour, IDragHandler {

        public Window window;

        public void OnDrag(PointerEventData eventData) {
            window.Resize(eventData.delta.x, eventData.delta.y);
        }
    }
}
