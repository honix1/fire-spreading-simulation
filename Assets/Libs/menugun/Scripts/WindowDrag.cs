﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MenuGun {
    public class WindowDrag : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler {

        public Window window;

        public void OnPointerClick(PointerEventData eventData) {
            window.ToTop();
        }

        public void OnBeginDrag(PointerEventData eventData) {
            window.ToTop();
        }

        public void OnDrag(PointerEventData eventData) {
            window.Move(eventData.delta.x, eventData.delta.y);
        }
    }
}