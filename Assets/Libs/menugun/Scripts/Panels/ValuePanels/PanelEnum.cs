﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelEnum : ValuePanel {

        public Dropdown dropdown;

        void SetValue(int value) {
            dropdown.value = value;
        }

        int GetValue() {
            return dropdown.value;
        }

        public override bool CanHandle(Type t) {
            return t.IsEnum;
        }

        public override void Subscribe(Action action) {
            dropdown.onValueChanged.AddListener((x) => action.Invoke());
        }

        public override void AfterLink() {
            dropdown.options = new List<string>(Enum.GetNames(linker.type)).ConvertAll(x => new Dropdown.OptionData(x));
            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((int)linker.GetValue());
        }
    }
}
