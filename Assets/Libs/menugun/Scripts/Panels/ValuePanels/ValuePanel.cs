﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class Linker {
        public Action<object> SetValue;
        public Func<object> GetValue;

        public Type type;
        public object[] customAttributes;
    }

    public abstract class ValuePanel : Panel {

        protected Linker linker;
        protected object linkedObject;

        public override void SetName(string name) {
            GetComponentInChildren<Text>().text = name;
        }

        public ValuePanel Link(object obj, MemberInfo mi) {
            GameObject go = Instantiate(gameObject, transform.parent);
            ValuePanel pn = go.GetComponent<ValuePanel>();

            pn.Identifier = mi.Name;

            pn.linkedObject = obj;
            if (mi is FieldInfo) {
                FieldInfo fi = mi as FieldInfo;
                Linker l   = new Linker();
                l.SetValue = (val) => fi.SetValue(obj, val);
                l.GetValue = () => fi.GetValue(obj);
                l.type     = fi.FieldType;
                l.customAttributes = fi.GetCustomAttributes(false);
                pn.linker = l;
            } else if (mi is PropertyInfo) {
                PropertyInfo pi = mi as PropertyInfo;
                Linker l   = new Linker();
                l.SetValue = (val) => pi.SetValue(obj, val, null);
                l.GetValue = () => pi.GetValue(obj, null);
                l.type     = pi.PropertyType;
                l.customAttributes = pi.GetCustomAttributes(false);
                pn.linker = l;
            } else {
                Debug.LogError("Not FieldInfo nor PropertyInfo");
            }

            pn.Fetch();

            pn.AfterLink();

            return pn;
        }

        abstract public bool CanHandle(Type type);
        abstract public void Subscribe(Action action);
        abstract public void AfterLink();
        abstract public void Fetch();
    }
}
