﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelString : ValuePanel {

        public InputField inputField;

        void SetValue(string value) {
            inputField.text = value;
        }

        string GetValue() {
            return inputField.text;
        }

        public override bool CanHandle(Type t) {
            return t == typeof(string);
        }

        public override void Subscribe(Action action) {
            inputField.onValueChanged.AddListener((x) => action.Invoke());
        }

        public override void AfterLink() {
            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((string)linker.GetValue());
        }
    }
}