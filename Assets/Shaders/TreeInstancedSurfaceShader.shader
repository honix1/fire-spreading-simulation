Shader "Instanced/TreeInstancedSurfaceShader" {
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_HealthColor("Health Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_FireColor("Fire Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_DeadColor("Dead Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model
		#pragma surface surf Standard addshadow fullforwardshadows
		#pragma multi_compile_instancing
		#pragma instancing_options procedural:setup

		sampler2D _MainTex;

		float4 _HealthColor;
		float4 _FireColor;
		float4 _DeadColor;

		struct Input {
			float2 uv_MainTex;
		};

	#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
		StructuredBuffer<float4> positionBuffer;
		StructuredBuffer<float2> firesBuffer;
	#endif

		void setup()
		{
		#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
			float4 data = positionBuffer[unity_InstanceID];

			float fire = firesBuffer[unity_InstanceID].x;
			float scale = data.w + fire;
			
			unity_ObjectToWorld._11_21_31_41 = float4(scale, 0, 0, 0);
			unity_ObjectToWorld._12_22_32_42 = float4(0, scale, 0, 0);
			unity_ObjectToWorld._13_23_33_43 = float4(0, 0, scale, 0);
			unity_ObjectToWorld._14_24_34_44 = float4(data.xyz, 1);
			unity_WorldToObject = unity_ObjectToWorld;
			unity_WorldToObject._14_24_34 *= -1;
			unity_WorldToObject._11_22_33 = 1.0f / unity_WorldToObject._11_22_33;
		#endif
		}

		half _Glossiness;
		half _Metallic;

		void surf(Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);

			float fire = 0, fuel = 0;

#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
			fire = firesBuffer[unity_InstanceID].x;
			fuel = firesBuffer[unity_InstanceID].y;
			c *= lerp(_DeadColor, lerp(_HealthColor, _FireColor, fire), fuel);
#endif

			o.Emission = _FireColor * fire;
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}