using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;

namespace FireSimulaiton.Test
{
	public class PipelineTest : MonoBehaviour
	{
		const int OBJECTS_COUNT = 100_000;

		private NativeArray<float> outputArray;
		private JobHandle testJobHandle;

		public NativeArray<float> GetArray()
		{
			return outputArray;
		}

		private void Awake()
		{
			outputArray = 
				new NativeArray<float>(OBJECTS_COUNT, Allocator.Persistent);
		}

		private void Update()
		{
			TestJob testJob = new TestJob()
			{
				output = outputArray,
				time = Time.time
			};

			testJobHandle = 
				testJob.Schedule(outputArray.Length, 1024, testJobHandle);

			// Call this when it's result needed
			testJobHandle.Complete();
		}

		private void OnDestroy()
		{
			outputArray.Dispose();
		}

		[BurstCompile]
		private struct TestJob : IJobParallelFor
		{
			[ReadOnly]
			public float time;

			[WriteOnly]
			public NativeArray<float> output;

			void IJobParallelFor.Execute(int index)
			{
				output[index] = 
					math.sin(time + (float)index / OBJECTS_COUNT * 2 * math.PI) / 2 + 0.25f;

				//output[index] = index % 2;
			}
		}
	}
}