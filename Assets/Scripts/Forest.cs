using MenuGun;
using System;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FireSimulaiton
{
	public class Forest : MonoBehaviour
	{
		public enum InteractionMode
		{
			AddTrees,
			RemoveTrees,
			FireTrees,
		}

		[SerializeField]
		private Terrain terrain;
		[SerializeField]
		private LayerMask groundLayerMask;
		[SerializeField]
		private Transform cursor;

		[MenuHeader("Trees")]
		public InteractionMode mode = InteractionMode.FireTrees;
		[MenuRange(100, 100_000)]
		public int targetCount = 50_000;

		[MenuHeader("Wind")]
		[MenuRange(0, 10)]
		public float windSpeed = 1f;
		[MenuRange(0, 360)]
		public float windDirection = 0;

		[MenuHeader("Simulation")]
		[MenuRange(0, 5)]
		public float simulationSpeed = 1;

		// Generation
		private List<Vector4> treePositionsSizes = new List<Vector4>(64_000);

		// Draw
		[SerializeField]
		private Mesh treeMesh;
		[SerializeField]
		private Material treeMaterial;

		private ComputeBuffer treePositionsGPU;
		private ComputeBuffer treeFiresGPU;

		private readonly uint[] bufferWithArgs = new uint[5] { 0, 0, 0, 0, 0 };
		private ComputeBuffer bufferWithArgsGPU;

		// Fire simulation
		private struct Tree
		{
			public float firePower;
			public float fuel;
		}

		private NativeArray<Tree> treeFires;

		private struct TreeNeighborhood
		{
			public Tree tree;
			public Vector3 direction;
		}

		private NativeArray<TreeNeighborhood> treeFireNeighborhoods;
		private int neighborhoodsPerTree = 8;

		private NativeArray<int> treeFireNeighborhoodsIndices;

		// Octree
		PointOctree<int> octree;
		List<int> octreeSearchList = new List<int>(32);

		// Debug
		private string labelMsg = "";

		// Unity events
		private void Start()
		{
			bufferWithArgsGPU = new ComputeBuffer(1, 5 * sizeof(uint),
				ComputeBufferType.IndirectArguments);
			RegenerateForest();
		}

		void OnDisable()
		{
			// TODO: shortcut with macro?

			if (treeFires.IsCreated)
			{
				treeFires.Dispose();
			}

			if (treeFireNeighborhoods.IsCreated)
			{
				treeFireNeighborhoods.Dispose();
			}

			if (treeFireNeighborhoodsIndices.IsCreated)
			{
				treeFireNeighborhoodsIndices.Dispose();
			}

			if (treePositionsGPU != null)
			{
				treePositionsGPU.Release();
			}

			if (bufferWithArgsGPU != null)
			{
				bufferWithArgsGPU.Release();
			}

			if (treeFiresGPU != null)
			{
				treeFiresGPU.Release();
			}
		}

		private readonly RaycastHit[] mouseHit = new RaycastHit[1];

		private void Update()
		{
			// Block raycasting if ui is used
			if (EventSystem.current.IsPointerOverGameObject()) return;

			float maxDistance = 1500;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
#if UNITY_EDITOR
			Debug.DrawRay(ray.origin, ray.direction * maxDistance, Color.blue);
#endif
			int hitsCount =
				Physics.RaycastNonAlloc(ray, mouseHit, maxDistance, groundLayerMask);

			if (hitsCount > 0)
			{
				cursor.position = mouseHit[0].point;
			}
			else
			{
				cursor.position = ray.GetPoint(maxDistance);
			}

			if (Input.GetMouseButton(0))
			{
				CursorHoldAction();
			}

			if (Input.GetMouseButtonUp(0))
			{
				CursorUpAction();
			}
		}

		private void LateUpdate()
		{
			UpdateTreeFires();
			UpdateTreeFiresGPU();

			Bounds bounds = terrain.terrainData.bounds;
			bounds.center += terrain.transform.position;

			Graphics.DrawMeshInstancedIndirect(
				mesh: treeMesh,
				submeshIndex: 0,
				material: treeMaterial,
				bounds: bounds,
				bufferWithArgs: bufferWithArgsGPU
			);
		}

		// Buttons
		public void ClearForest()
		{
			treePositionsSizes.Clear();

			UpdateBufferWithArgsGPU();
		}

		public void RegenerateForest()
		{
			GenerateTreePositionsSizes();
			UpdateBufferWithArgsGPU();
			LoadTreePositionsSizesGPU();

			GenerateTreeFires();
			GenerateTreeFiresGPU();
		}

		public void FireRandomTrees()
		{
			for (int i = 0; i < treeFires.Length; i++)
			{
				if (UnityEngine.Random.value > 0.999f) // Select rarely
				{
					var treeFire = treeFires[i];
					treeFire.firePower = 1f;
					treeFires[i] = treeFire;
				}
			}
		}

		public void Quit()
		{
			Application.Quit();
		}

		// Private methods
		private void CursorHoldAction()
		{
			Vector3 cursorPos = cursor.position;
			float cursorRadius = cursor.localScale.x / 2;

			switch (mode)
			{
				case InteractionMode.AddTrees:
					// WARN: unoptimized code
					IEnumerable<Vector4> terrainPoints =
						TerrainPoints(new Vector3Int(50, 1, 50));

					foreach (Vector4 positionSize in terrainPoints)
					{
						if (Vector3.Distance(positionSize, cursorPos) < cursorRadius)
						{
							treePositionsSizes.Add(positionSize);
						}
					}

					break;

				case InteractionMode.RemoveTrees:
					// WARN: much allocations
					List<Vector4> newTreePositionsSizes =
						new List<Vector4>(64_000);

					for (int i = 0; i < treePositionsSizes.Count; i++)
					{
						Vector4 positionSize = treePositionsSizes[i];
						if (Vector3.Distance(positionSize, cursorPos) > cursorRadius)
						{
							newTreePositionsSizes.Add(positionSize);
						}
						else if (UnityEngine.Random.value > 0.1f) // Smooth remove
						{
							newTreePositionsSizes.Add(positionSize);
						}
					}

					treePositionsSizes = newTreePositionsSizes;

					break;

				case InteractionMode.FireTrees:
					octreeSearchList.Clear();
					if (octree.GetNearbyNonAlloc(cursorPos, cursorRadius, octreeSearchList))
					{
						foreach (int i in octreeSearchList)
						{
							if (UnityEngine.Random.value > 0.99f) // Smooth fire
							{
								var treeFire = treeFires[i];
								treeFire.firePower = 1f;
								treeFires[i] = treeFire;
							}
						}
					}
					break;
			}

			if (treePositionsSizes.Count > 0)
			{
				// Update only visuals
				UpdateBufferWithArgsGPU();
				LoadTreePositionsSizesGPU();
			}
		}

		private void CursorUpAction()
		{
			switch (mode)
			{
				case InteractionMode.AddTrees:
				case InteractionMode.RemoveTrees:
					// Full update
					if (treePositionsSizes.Count > 0)
					{
						OctreePopulation();
						UpdateBufferWithArgsGPU();
						LoadTreePositionsSizesGPU();
						GenerateTreeFires();
						GenerateTreeFiresGPU();
					}
					break;
			}
		}

		// Tree posiotions
		private IEnumerable<Vector4> TerrainPoints(Vector3Int resolution)
		{
			int count = resolution.x * resolution.y * resolution.z;

			float gridStep = 1f / resolution.x;

			var rayCastCommands =
				new NativeArray<RaycastCommand>(count, Allocator.TempJob);
			int rayCastCommandsPointer = 0;

			IEnumerable<Vector3> gridGenerator =
				Utils.GridCube(resolution);

			foreach (Vector3 point in gridGenerator)
			{
				Vector3 rayOrigin =
					point + Utils.RandomVector * gridStep;

				rayOrigin =
					Vector3.Scale(rayOrigin, terrain.terrainData.size) +
					terrain.GetPosition();

				rayOrigin += Vector3.up * 500;

				rayCastCommands[rayCastCommandsPointer++] =
					new RaycastCommand(
						rayOrigin, Vector3.down,
						distance: 1000, groundLayerMask);
			}

			var results = new NativeArray<RaycastHit>(count, Allocator.TempJob);

			JobHandle handle =
				RaycastCommand.ScheduleBatch(rayCastCommands, results, minCommandsPerJob: 128);
			handle.Complete();

			foreach (RaycastHit hit in results)
			{
				if (hit.collider == null) continue;

				Vector4 positionSize = hit.point;
				positionSize.w = UnityEngine.Random.Range(0.75f, 1.5f);
				yield return positionSize;
			}

			rayCastCommands.Dispose();
			results.Dispose();
		}

		private void GenerateTreePositionsSizes()
		{
			int squareRoot = (int)Mathf.Sqrt(targetCount);

			treePositionsSizes.Clear();
			treePositionsSizes.AddRange(
				TerrainPoints(new Vector3Int(squareRoot, 1, squareRoot)));

			OctreePopulation();
		}

		private void OctreePopulation()
		{
			// TODO: get better terrain size
			octree = new PointOctree<int>(
				terrain.terrainData.size.x,
				terrain.GetPosition(),
				16);

			for (int i = 0; i < treePositionsSizes.Count; i++)
			{
				octree.Add(i, treePositionsSizes[i]);
			}
		}

		// Tree fires
		private void GenerateTreeFires()
		{
			if (treeFires.IsCreated)
			{
				treeFires.Dispose();
			}

			treeFires = new NativeArray<Tree>(
				treePositionsSizes.Count,
				Allocator.Persistent,
				NativeArrayOptions.ClearMemory);

			for (int i = 0; i < treePositionsSizes.Count; i++)
			{
				treeFires[i] = new Tree
				{
					fuel = UnityEngine.Random.Range(8f, 16f)
				};
			}

			if (treeFireNeighborhoods.IsCreated)
			{
				treeFireNeighborhoods.Dispose();
			}

			treeFireNeighborhoods = new NativeArray<TreeNeighborhood>(
				treePositionsSizes.Count * neighborhoodsPerTree,
				Allocator.Persistent,
				NativeArrayOptions.ClearMemory);

			if (treeFireNeighborhoodsIndices.IsCreated)
			{
				treeFireNeighborhoodsIndices.Dispose();
			}

			treeFireNeighborhoodsIndices = new NativeArray<int>(
				treePositionsSizes.Count * neighborhoodsPerTree,
				Allocator.Persistent,
				NativeArrayOptions.ClearMemory);

			// Calculate treeFireNeighborhoodsIndices and treeFireNeighborhoods
			int dynamicDistanceTap = 8;
			for (int i = 0; i < treePositionsSizes.Count; i++)
			{
				Vector3 thisTreePos = treePositionsSizes[i];
				
				octreeSearchList.Clear();
				if (octree.GetNearbyNonAlloc(thisTreePos, dynamicDistanceTap, octreeSearchList))
				{
					octreeSearchList.Sort((x, y) => (int)(
						(thisTreePos - (Vector3)treePositionsSizes[x]).sqrMagnitude -
						(thisTreePos - (Vector3)treePositionsSizes[y]).sqrMagnitude));

					int foundCount = 
						Mathf.Min(neighborhoodsPerTree, octreeSearchList.Count - 1); // -1 self

					if (foundCount < neighborhoodsPerTree)
					{
						dynamicDistanceTap++;
					}
					else if (foundCount > neighborhoodsPerTree)
					{
						dynamicDistanceTap--;
					}

					for (int j = 0, s = 0; j < foundCount; j++, s++)
					{
						Vector3 otherTreePos = treePositionsSizes[octreeSearchList[s]];
						Vector3 posDiff = thisTreePos - otherTreePos;

						if (posDiff == Vector3.zero)
						{
							// Skip self, but continue with same index
							j--;
							continue;
						}

						int treeShift = i * neighborhoodsPerTree;
						int index = treeShift + j;

						treeFireNeighborhoodsIndices[index] = octreeSearchList[s];
						treeFireNeighborhoods[index] = new TreeNeighborhood
						{
							direction = posDiff
						};
					}
				}
			}
		}

		[BurstCompile]
		private struct CopyTreeFiresToTreeFireNeighborhoods : IJob
		{
			[ReadOnly]
			public NativeArray<Tree> treeFires;
			[ReadOnly]
			public NativeArray<int> treeFireNeighborhoodsIndices;
			// ReadWrite, but can be WriteOnly
			public NativeArray<TreeNeighborhood> treeFireNeighborhoods;

			[ReadOnly]
			public int neighborhoodsPerTree;

			public void Execute()
			{
				for (int i = 0; i < treeFires.Length; i++)
				{
					for (int j = 0; j < neighborhoodsPerTree; j++)
					{
						int treeShift = i * neighborhoodsPerTree;
						int iindex = treeShift + j;
						int nindex = treeFireNeighborhoodsIndices[iindex];

						TreeNeighborhood treeN = treeFireNeighborhoods[iindex];
						treeN.tree = treeFires[nindex];
						treeFireNeighborhoods[iindex] = treeN;
					}
				}
			}
		}

		[BurstCompile]
		private struct UpdateTreeFiresUsingTreeFireNeighborhoods : IJobParallelFor
		{
			// ReadWrite, but can be WriteOnly
			public NativeArray<Tree> treeFires;
			[ReadOnly]
			public NativeArray<TreeNeighborhood> treeFireNeighborhoods;

			[ReadOnly]
			public int neighborhoodsPerTree;
			[ReadOnly]
			public Vector3 wind;
			[ReadOnly]
			public float deltaTime;

			public void Execute(int i)
			{
				float accFire = 0;

				for (int j = 0; j < neighborhoodsPerTree; j++)
				{
					int treeShift = i * neighborhoodsPerTree;
					int index = treeShift + j;

					TreeNeighborhood neighborhood = treeFireNeighborhoods[index];

					float dot = math.dot(wind, neighborhood.direction);

					// TODO: Add random?
					// Propagate more fire if aligned withwind direction,
					// but strong wind will stop fire spreading.
					accFire +=
						neighborhood.tree.firePower *
						(0.25f + math.max(0, dot) - math.lengthsq(wind));
				}

				Tree tree = treeFires[i];
				tree.firePower =
					math.saturate(tree.fuel * 2) *
					math.saturate(tree.firePower + accFire * 0.1f * deltaTime);
				tree.fuel =
					math.max(0, tree.fuel - tree.firePower * deltaTime);
				treeFires[i] = tree;
			}
		}

		private void UpdateTreeFires()
		{
			{
				JobHandle handle = new CopyTreeFiresToTreeFireNeighborhoods
				{
					treeFires = treeFires,
					treeFireNeighborhoodsIndices = treeFireNeighborhoodsIndices,
					treeFireNeighborhoods = treeFireNeighborhoods,
					neighborhoodsPerTree = neighborhoodsPerTree
				}.Schedule();
				handle.Complete();
			}

			{
				JobHandle handle = new UpdateTreeFiresUsingTreeFireNeighborhoods
				{
					treeFires = treeFires,
					treeFireNeighborhoods = treeFireNeighborhoods,
					neighborhoodsPerTree = neighborhoodsPerTree,
					wind = Quaternion.Euler(Vector3.up * windDirection) * Vector3.forward * windSpeed,
					deltaTime = Time.deltaTime * simulationSpeed
				}.Schedule(treeFires.Length, 128);
				handle.Complete();
			}
		}

		private void GenerateTreeFiresGPU()
		{
			if (treeFiresGPU != null)
			{
				treeFiresGPU.Release();
			}

			int floatSize = 4;
			int stride = floatSize * 2;
			treeFiresGPU = new ComputeBuffer(
				treeFires.Length, stride,
				ComputeBufferType.Default, ComputeBufferMode.SubUpdates);

			treeMaterial.SetBuffer("firesBuffer", treeFiresGPU);
		}

		[BurstCompile]
		private struct CopyTreeFiresToGPU : IJobParallelFor
		{
			[ReadOnly]
			public NativeArray<Tree> treeFires;
			[WriteOnly]
			public NativeArray<float2> treeFiresGPUWrite;

			public void Execute(int i)
			{
				treeFiresGPUWrite[i] = new Vector2(
					math.saturate(treeFires[i].firePower),
					math.saturate(treeFires[i].fuel)
				);
			}
		}

		private void UpdateTreeFiresGPU()
		{
			var treeFiresGPUWrite = treeFiresGPU.BeginWrite<float2>(0, treeFires.Length);

			JobHandle handle = new CopyTreeFiresToGPU
			{
				treeFires = treeFires,
				treeFiresGPUWrite = treeFiresGPUWrite
			}.Schedule(treeFires.Length, 128);
			handle.Complete();

			treeFiresGPU.EndWrite<float2>(treeFires.Length);
		}

		// Graphics
		private void UpdateBufferWithArgsGPU()
		{
			if (treeMesh != null)
			{
				bufferWithArgs[0] = (uint)treeMesh.GetIndexCount(0);
				bufferWithArgs[1] = (uint)treePositionsSizes.Count;
				bufferWithArgs[2] = (uint)treeMesh.GetIndexStart(0);
				bufferWithArgs[3] = (uint)treeMesh.GetBaseVertex(0);
			}
			else
			{
				bufferWithArgs.Initialize();
			}
			bufferWithArgsGPU.SetData(bufferWithArgs);
		}

		private void LoadTreePositionsSizesGPU()
		{
			if (treePositionsGPU != null)
			{
				treePositionsGPU.Release();
			}

			int floatSize = 4;
			int stride = floatSize * 4;
			treePositionsGPU = new ComputeBuffer(treePositionsSizes.Count, stride);

			treePositionsGPU.SetData(treePositionsSizes);
			treeMaterial.SetBuffer("positionBuffer", treePositionsGPU);

			labelMsg = $"Trees count {treePositionsSizes.Count}";
		}


		// Info
		void OnGUI()
		{
			GUILayout.Label(labelMsg);
		}

		void OnDrawGizmos()
		{
			if (octree != null)
			{
				octree.DrawAllBounds(); // Draw node boundaries
				octree.DrawAllObjects(); // Mark object positions
			}
		}
	}
}