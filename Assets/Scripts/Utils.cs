﻿using System.Collections.Generic;
using UnityEngine;

namespace FireSimulaiton
{
	public class Utils
	{
		/// <summary>
		/// Util to create spatial grid of points.
		/// </summary>
		/// <param name="resolution">Resolution of grid.</param>
		/// <returns>Sequence of points of grid.</returns>
		public static IEnumerable<Vector3> GridCube(Vector3Int resolution)
		{
			for (int z = 0; z < resolution.x; z++)
				for (int y = 0; y < resolution.y; y++)
					for (int x = 0; x < resolution.x; x++)
						yield return new Vector3(
							(float)x / resolution.x,
							(float)y / resolution.y,
							(float)z / resolution.z);
		}

		public static Vector3 RandomVector => 
			new Vector3(
				Random.Range(-1f, 1f),
				Random.Range(-1f, 1f),
				Random.Range(-1f, 1f)
			);
	}
}