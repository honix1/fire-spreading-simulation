using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FireSimulaiton
{
    public class WindArrow : MonoBehaviour
    {
        [SerializeField]
        private Forest forest;

        void Update()
        {
            transform.rotation = 
                Quaternion.Euler(Vector3.up * forest.windDirection);
            transform.localScale = 
                new Vector3(1, 1, forest.windSpeed);
        }
    }
}